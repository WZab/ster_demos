#!/bin/bash
/tmp/qemu/bin/qemu-system-x86_64 --device pci-wzenc1 --device pci-wzadc1 --device pci-wzcom1 -m 8192 -enable-kvm -cdrom /home/wzab/knoppix/8.2/KNOPPIX*.iso \
 -display sdl \
 -netdev user,id=eth0 -device rtl8139,netdev=eth0 \
 -fsdev local,id=h9,path=/tmp/wspolny,security_model=none \
 -device virtio-9p-pci,fsdev=h9,mount_tag=h
