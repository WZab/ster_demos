/*
  This is a very simple program showing functionality of the WZENC1 device model
*/
#include<stdio.h>
#include<sys/types.h>
#include<stdint.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<fcntl.h>
#include <unistd.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include "wzab_enc1.h"

unsigned char key1[]="01234567abcdefgh01234567hgfedcba";
unsigned char    iv1[]="dferrtertertrtr34gfdfge3t3434334";
unsigned char text1[]="This is a secret message. We will encrypt it and later decrypt it back to show operation of WZENC1.";
unsigned char text2[]="This is the second segment of our top secret message. It will be passed through the second page.";

volatile WzEnc1Regs * regs = NULL;
int plik = -1;
volatile uint32_t * devm1;
volatile unsigned char * devm2;

void print_char(unsigned char c)
{
  if((c>27) && (c<128)) {
    printf("%c",c);
  } else {
    printf("<%2.2x>",(int)c);
  }
}
void print_texts()
{
  int i;
  for(i=0;i<sizeof(text1);i++) print_char(text1[i]);
  for(i=0;i<sizeof(text2);i++) print_char(text2[i]);
}

void encrypt_or_decrypt(int operation)
{
  uint32_t status;
  //Clear and unamsk interrupt
  regs->Ctrl = ENC1_CMD_ACKIRQ;
  //Initialize the key
  memcpy((void *)&devm2[0],key1,32);
  //Initialize the IV
  memcpy((void *)&devm2[4096],iv1,32);
  //Select encryption or encryption
  if(operation) 
    regs->Ctrl = ENC1_CMD_ENCR;
  else 
    regs->Ctrl = ENC1_CMD_DECR;
  //Prepare data
  regs->Pages[0].Offset=0;
  regs->Pages[0].Length=sizeof(text1);
  memcpy((void *)&devm2[0],text1,sizeof(text1));
  regs->Pages[1].Offset=0;
  regs->Pages[1].Length=sizeof(text2);
  memcpy((void *)&devm2[0x1000],text2,sizeof(text2));
  regs->Pages[2].Length=0; //No data on page 2
  regs->Ctrl = ENC1_CMD_DATA;
  //Read - wait until ready
  printf("waiting for IRQ\n");
  fflush(stdout);
  if(read(plik,&status,4)!=4) exit(1);
  printf("received IRQ, status %x\n", status);
  fflush(stdout);
  //Save processed data
  memcpy(text1,(void *)devm2,sizeof(text1));
  memcpy(text2,(void *)&devm2[0x1000],sizeof(text2));
  //Free WZENC1
  regs->Ctrl = ENC1_CMD_STOP;
}

int main()
{
  unsigned long val;
  int i;
  printf("I'm trying to open our device!\n");
  fflush(stdout);
  plik=open("/dev/my_enc0", O_RDWR | O_DSYNC | O_SYNC);
  if(plik==-1)
    {
      printf("I can't open device!\n");
      fflush(stdout);
      exit(1);
    }
  printf("Device opened!\n");
  fflush(stdout);
  //Map registers (in fact it should not be necessary!
  devm1 = (uint32_t *) mmap(0,0x1000,PROT_READ | PROT_WRITE,MAP_SHARED,
	 plik,0x0000000);
  if(devm1 == (void *) -1l)
    {
      perror("I can't map registers\n");
    }
  regs = (volatile WzEnc1Regs *) devm1;
  //Map data buffer
  devm2 = (unsigned char *) mmap(0,0x4000,PROT_READ | PROT_WRITE,MAP_SHARED,
	 plik,0x0001000);
  if(devm2 == (void *) -1l)
    {
      perror("I can't map data buffer!\n");
    }
  printf("Memory mapped: regs: %x buffer: %x\n",devm1, devm2);
  print_texts(); //Print original text
  fflush(stdout);
  encrypt_or_decrypt(1);
  print_texts(); //Print encrypted text
  fflush(stdout);
  encrypt_or_decrypt(0);
  print_texts(); //Print decrypted text
  fflush(stdout);
}
