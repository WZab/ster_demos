/* Quick and dirty WZENC1 device driver
 * This driver only allows to check, that the simulated
 * WZENC1 core works correctly.
 * Most functionality is simply passed to the user space
 * allowing you to use WZENC1 to completely crash your emulated
 * machine :-(.
 * This driver also does not allow to control multiple
 * instances of WZENC1
 * 
 * Copyright (C) 2011 by Wojciech M. Zabolotny
 * wzab<at>ise.pw.edu.pl
 * Significantly based on multiple drivers included in
 * sources of Linux
 * Therefore this saource is licensed under GPL v2
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <asm/uaccess.h>
MODULE_LICENSE("GPL v2");
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include "wzab_enc1.h"


#define SUCCESS 0
#define DEVICE_NAME "wzab_enc1"

int irq=-1; //IRQ used by WZENC1 by default we use irq 3
unsigned long phys_addr = 0;
//Global variables used to store information about WZENC1
//This must be changed, if we'd like to handle multiple WZENC1 instances

volatile uint32_t * fmem=NULL; //Pointer to registers area
volatile void * fdata=NULL; //Pointer to data buffer

void cleanup_tst1( void );
void cleanup_tst1( void );
int init_tst1( void );
static int tst1_open(struct inode *inode, struct file *file);
static int tst1_release(struct inode *inode, struct file *file);
ssize_t tst1_read(struct file *filp,
		  char __user *buf,size_t count, loff_t *off);
ssize_t tst1_write(struct file *filp,
		   const char __user *buf,size_t count, loff_t *off);
loff_t tst1_llseek(struct file *filp, loff_t off, int origin);

int tst1_mmap(struct file *filp, struct vm_area_struct *vma);

dev_t my_dev=0;
struct cdev * my_cdev = NULL;
static struct class *class_my_tst = NULL;

/* Queue for reading process */
DECLARE_WAIT_QUEUE_HEAD (readqueue);

/* Interrupt service routine */
irqreturn_t tst1_irq(int irq, void * dev_id)
{
  // First we check if our device requests interrupt
  //printk("<1>I'm in interrupt!\n");
  volatile uint32_t status; //Must be volatile to ensure 32-bit access!
  WzEnc1Regs * volatile regs;
  regs = (WzEnc1Regs * volatile) fmem;
  status = regs->Status;
  if(status & 0x8000) {
    //Yes, this is our device
    //Block interrupts, they will be turned on by the reading process
    regs->Ctrl = ENC1_CMD_DISIRQ;
    //Wake up the reading process
    wake_up_interruptible(&readqueue);
    return IRQ_HANDLED;
  }
  return IRQ_NONE; //Our device does not request interrupt
};


struct file_operations Fops = {
  .owner = THIS_MODULE,
  .read=tst1_read, /* read */
  .write=tst1_write, /* write */
  .open=tst1_open,
  .release=tst1_release,  /* a.k.a. close */
  .llseek=no_llseek,
  .mmap=tst1_mmap
};

/* Cleanup resources */
int tst1_remove( struct platform_device * pdev )
{
  if(my_dev && class_my_tst) {
    device_destroy(class_my_tst,my_dev);
  }
  if(fdata) free_pages((unsigned long)fdata,2);
  if(fmem) iounmap(fmem);
  if(my_cdev) cdev_del(my_cdev);
  my_cdev=NULL;
  unregister_chrdev_region(my_dev, 1);
  if(class_my_tst) {
    class_destroy(class_my_tst);
    class_my_tst=NULL;
  }
  //printk("<1>drv_tst1 removed!\n");
  return SUCCESS;
}

static int tst1_open(struct inode *inode, 
		     struct file *file)
{
  int res=0;
  nonseekable_open(inode, file);
  res=request_irq(irq,tst1_irq,0,DEVICE_NAME,NULL); //Should be changed for multiple WZENC1s
  if(res) {
    printk (KERN_INFO "wzab_tst1: I can't connect irq %i error: %d\n", irq,res);
    irq = -1;
  }
  return SUCCESS;
}

static int tst1_release(struct inode *inode, 
			struct file *file)
{
  WzEnc1Regs * volatile regs;
  regs = (WzEnc1Regs * volatile) fmem;
#ifdef DEBUG
  printk ("<1>device_release(%p,%p)\n", inode, file);
#endif
  regs->Ctrl = ENC1_CMD_DISIRQ; //Disable IRQ
  if(irq>=0) free_irq(irq,NULL); //Free interrupt
  return SUCCESS;
}

static inline uint32_t read_status(void)
{
  WzEnc1Regs * volatile regs;
  volatile uint32_t status;
  regs = (WzEnc1Regs * volatile) fmem;
  status  = regs->Status;
  return status;
}

ssize_t tst1_read(struct file *filp,
		  char __user *buf,size_t count, loff_t *off)
{
  uint32_t val;
  if (count != 4) return -EINVAL; //Only 4-byte accesses allowed
  {
    ssize_t res;
    //Interrupts are on, so we should sleep and wait for interrupt
    res=wait_event_interruptible(readqueue,read_status() & 0x8000);
    if(res) return res; //Signal received!
  }
  //Read pointers 
  val = read_status(); 
  if(__copy_to_user(buf,&val,4)) return -EFAULT;
  return 4;
}

ssize_t tst1_write(struct file *filp,
		   const char __user *buf,size_t count, loff_t *off)
{
  //uint32_t val;
  if (count != 4) return -EINVAL; //Only 4-byte access allowed
  //__copy_from_user(&val,buf,4);
  //Write is not used at all!
  return 4;
}	


void tst1_vma_open (struct vm_area_struct * area)
{  }

void tst1_vma_close (struct vm_area_struct * area)
{  }

static struct vm_operations_struct tst1_vm_ops = {
  .open=tst1_vma_open,
  .close=tst1_vma_close,
};

int tst1_mmap(struct file *filp,
	      struct vm_area_struct *vma)
{
  unsigned long off = vma->vm_pgoff << PAGE_SHIFT;
  if(off==0) {
    //Mapping of registers
    unsigned long physical = phys_addr;
    unsigned long vsize = vma->vm_end - vma->vm_start;
    unsigned long psize = 0x1000; //One page is enough
    //printk("<1>start mmap of registers\n");
    if(vsize>psize)
      return -EINVAL;
    remap_pfn_range(vma,vma->vm_start, physical >> PAGE_SHIFT , vsize, vma->vm_page_prot);
    if (vma->vm_ops)
      return -EINVAL; //It should never happen
    vma->vm_ops = &tst1_vm_ops;
    tst1_vma_open(vma); //This time no open(vma) was called
    //printk("<1>mmap of registers succeeded!\n");
    return 0;
  } else {
    //Mapping of buffer 
    //In the newest kernel we may use remap_pfn_range to map RAM.
    unsigned long physical = virt_to_phys(fdata);
    unsigned long vsize = vma->vm_end - vma->vm_start;
    unsigned long psize = 4*4096; //4 pages of buffer!
    //printk("<1>start mmap of buffers\n");
    if(vsize>psize)
      return -EINVAL;
    remap_pfn_range(vma,vma->vm_start, physical >> PAGE_SHIFT , vsize, vma->vm_page_prot);
    if (vma->vm_ops)
      return -EINVAL; //It should never happen
    vma->vm_ops = &tst1_vm_ops;
    tst1_vma_open(vma); //This time no open(vma) was called
    //printk("<1>mmap of buffers succeeded!\n");
    return 0;
  }
}

static int tst1_probe(struct platform_device * pdev)
{
  int res = 0;
  struct resource * resptr = NULL;
  irq = platform_get_irq(pdev,0);
  if(irq<0) {
    printk(KERN_ERR "Error reading the IRQ number: %d.\n",irq);
    res=irq;
    goto err1;
  }
  printk(KERN_ALERT "Connected IRQ=%d\n",irq);
  resptr = platform_get_resource(pdev,IORESOURCE_MEM,0);
  if(resptr==0) {
    printk(KERN_ERR "Error reading the register addresses.\n");
    res=-EINVAL;
    goto err1;
  }
  phys_addr = resptr->start; //We don't check the size!
  printk(KERN_ALERT "Connected registers at %lx\n",phys_addr);
  class_my_tst = class_create(THIS_MODULE, "my_tst");
  if (IS_ERR(class_my_tst)) {
    printk(KERN_ERR "Error creating my_tst class.\n");
    res=PTR_ERR(class_my_tst);
    goto err1;
  }
  /* Alocate device number */
  res=alloc_chrdev_region(&my_dev, 0, 1, DEVICE_NAME);
  if(res) {
    printk ("<1>Alocation of the device number for %s failed\n",
            DEVICE_NAME);
    goto err1; 
  };
  my_cdev = cdev_alloc( );
  if(my_cdev == NULL) {
    printk ("<1>Allocation of cdev for %s failed\n",
            DEVICE_NAME);
    goto err1;
  }
  my_cdev->ops = &Fops;
  my_cdev->owner = THIS_MODULE;
  /* Add character device */
  res=cdev_add(my_cdev, my_dev, 1);
  if(res) {
    printk ("<1>Registration of the device number for %s failed\n",
            DEVICE_NAME);
    goto err1;
  };
  /* Create pointer needed to access registers */
  fmem = ioremap(phys_addr, 0x1000); //One page should be enough
  if(!fmem) {
    printk ("<1>Mapping of memory for %s registers failed\n",
	    DEVICE_NAME);
    res= -ENOMEM;
    goto err1;
  }
  /* Create pointer needed to access ADC data */
  fdata = (void *) __get_free_pages(GFP_KERNEL, 2);
  if(!fdata) {
    printk ("<1>Mapping of memory for %s data failed\n",
	    DEVICE_NAME);
    res= -ENOMEM;
    goto err1;
  }
  /* Write physical addresses of buffer pages to registers */
  {
    int i;
    WzEnc1Regs * volatile regs;
    regs = (WzEnc1Regs * volatile) fmem;
    for(i=0;i<4;i++) {
      regs->Pages[i].PhysAddr = virt_to_phys(fdata+4096*i);
      regs->Pages[i].Length = 0;
      regs->Pages[i].Offset = 0;
    }
  }
  device_create(class_my_tst,NULL,my_dev,NULL,"my_enc%d",MINOR(my_dev));
  printk ("<1>%s The major device number is %d.\n",
	  "Registeration is a success.",
	  MAJOR(my_dev));
  return 0;
 tst1_remove(pdev);
 err1:
  return res;
}

static struct of_device_id enc1_driver_ids[] = {
  {
    .compatible = "wzab_enc1",
  },
  {},
};
struct platform_driver my_driver = {
	.driver = { 
           .name = "wzab-enc1",
           .of_match_table = enc1_driver_ids,
         },
	.probe = tst1_probe,
	.remove = tst1_remove,
};

static int my_init(void)
{
  int ret = platform_driver_register(&my_driver);
  if (ret < 0) {
     printk(KERN_ERR "Failed to register my platform driver: %d\n",ret);
         return ret;
  }
  printk(KERN_ALERT "Witam serdecznie\n");
  return 0;
}
static void my_exit(void)
{
  printk(KERN_ALERT "Do widzenia\n");
}

module_init(my_init);
module_exit(my_exit);

