/* Very simplified WZCOM1 device driver
 * This driver does not allow to control multiple
 * instances of WZCOM1
 *
 * Copyright (C) 2018 by Wojciech M. Zabolotny
 * wzab<at>ise.pw.edu.pl
 * Significantly based on multiple drivers included in
 * sources of Linux
 * Therefore this source is licensed under GPL v2
 */

#include <linux/kernel.h>
#include <linux/module.h>
//#include <asm/uaccess.h>
MODULE_LICENSE("GPL v2");
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/pci.h>
#include <asm/io.h>
#include <linux/interrupt.h>
#include <linux/uaccess.h>
#include <linux/platform_device.h>
#include <linux/of.h>
#include <linux/kfifo.h>
#include "wzab_sg_com1.h"


#define SUCCESS 0
#define DEVICE_NAME "wzab_com1"

//PCI IDs below are not registred! Use only for experiments!
#define PCI_VENDOR_ID_WZAB 0xabba
#define PCI_DEVICE_ID_WZAB_WZCOM1 0x0125

static const struct pci_device_id tst1_pci_tbl[] = {
    {PCI_DEVICE(PCI_VENDOR_ID_WZAB, PCI_DEVICE_ID_WZAB_WZCOM1)},
    {}
};
MODULE_DEVICE_TABLE(pci, tst1_pci_tbl);

//Global variables used to store information about WZCOM1
//This must be changed, if we'd like to handle multiple WZCOM1 instances
int irq=0; //IRQ used by WZCOM1
unsigned long phys_addr = 0; //Physical address of register memory
volatile uint32_t * fmem=NULL; //Pointer to registers area
volatile void * fdata=NULL; //Pointer to data buffer
struct pci_dev * my_pdev;

DECLARE_KFIFO(rd_fifo,uint64_t,128);

void cleanup_tst1( void );
void cleanup_tst1( void );
int init_tst1( void );
static int tst1_open(struct inode *inode, struct file *file);
static int tst1_release(struct inode *inode, struct file *file);

/*ssize_t tst1_read(struct file *filp,
		  char __user *buf,size_t count, loff_t *off);
int tst1_mmap(struct file *filp, struct vm_area_struct *vma);
*/
ssize_t tst1_write(struct file *filp,
                   const char __user *buf,size_t count, loff_t *off);

static long tst1_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
int is_open = 0; //Flag informing if the device is open
dev_t my_dev=0;
struct cdev * my_cdev = NULL;
static struct class *class_my_tst = NULL;

/* Queue for reading process */
DECLARE_WAIT_QUEUE_HEAD (readqueue);

/* Interrupt service routine */
irqreturn_t tst1_irq(int irq, void * dev_id)
{
    // First we check if our device requests interrupt
    //printk("<1>I'm in interrupt!\n");
    volatile uint32_t status; //Must be volatile to ensure 32-bit access!
    uint64_t val;
    volatile WzCom1Regs * regs;
    regs = (volatile WzCom1Regs *) fmem;
    status = regs->Status;
    if(status & 0x80000000) {
        //Yes, our device requests service
        //Read the counter
        wake_up_interruptible(&readqueue);
        return IRQ_HANDLED;
    }
    return IRQ_NONE; //Our device does not request interrupt
};


struct file_operations Fops = {
    .owner = THIS_MODULE,
    //.read=tst1_read, /* read */
    .write=tst1_write, /* write */
    .open=tst1_open,
    .release=tst1_release,  /* a.k.a. close */
    .llseek=no_llseek,
    .unlocked_ioctl=tst1_ioctl,
    .compat_ioctl=tst1_ioctl,
    //.mmap = tst1_mmap,
};

/* Cleanup resources */
void tst1_remove( struct pci_dev * pdev )
{
    pci_release_regions(pdev);
    pci_disable_device(pdev);
    if(my_dev && class_my_tst) {
        device_destroy(class_my_tst,my_dev);
    }
    if(fdata)
        free_pages((unsigned long)fdata,2);
    if(fmem)
        iounmap(fmem);
    if(my_cdev)
        cdev_del(my_cdev);
    my_cdev=NULL;
    unregister_chrdev_region(my_dev, 1);
    if(class_my_tst) {
        class_destroy(class_my_tst);
        class_my_tst=NULL;
    }
    //printk("<1>drv_tst1 removed!\n");
    if(my_pdev == pdev)
        my_pdev = NULL;
}

static int tst1_open(struct inode *inode,
                     struct file *file)
{
    int res=0;
    volatile WzCom1Regs * regs;
    if(is_open)
        return -EBUSY; //May be opened only once!
    regs = (volatile WzCom1Regs *) fmem;
    nonseekable_open(inode, file);
    kfifo_reset(&rd_fifo); //Remove
    //res=request_irq(irq,tst1_irq,0,DEVICE_NAME,NULL); //Should be changed for multiple WZCOM1s
    res=request_irq(irq,tst1_irq, IRQF_SHARED | IRQF_NO_THREAD,DEVICE_NAME,file);  //Should be changed for multiple WZCOM1s
    if(res) {
        printk (KERN_INFO "wzab_com1: I can't connect irq %i error: %d\n", irq,res);
        irq = -1;
    }
    //regs->stat = 1; //Unmask interrupts

    return SUCCESS;
}

static int tst1_release(struct inode *inode,
                        struct file *file)
{
    volatile WzCom1Regs *  regs;
    regs = (volatile WzCom1Regs *) fmem;
#ifdef DEBUG
    printk ("<1>device_release(%p,%p)\n", inode, file);
#endif
    regs->Ctrl = COM1_CMD_DISIRQ;
    if(irq>=0)
        free_irq(irq,file); //Free interrupt
    is_open=0;
    return SUCCESS;
}

/*
ssize_t tst1_read(struct file *filp,
		  char __user *buf,size_t count, loff_t *off)
{
  uint64_t val;
  if (count != 8) return -EINVAL; //Only 8-byte accesses allowed
  {
    ssize_t res;
    //Interrupts are on, so we should sleep and wait for interrupt
    res=wait_event_interruptible(readqueue,!kfifo_is_empty(&rd_fifo));
    if(res) return res; //Signal received!
  }
  //Read pointers
  if(!kfifo_get(&rd_fifo,&val)) return -EINVAL;
  if(_copy_to_user(buf,&val,8)) return -EFAULT;
  return 8;
}

void tst1_vma_open (struct vm_area_struct * area)
{  }

void tst1_vma_close (struct vm_area_struct * area)
{  }

static struct vm_operations_struct tst1_vm_ops = {
  .open=tst1_vma_open,
  .close=tst1_vma_close,
};

int tst1_mmap(struct file *filp,
	      struct vm_area_struct *vma)
{
    //unsigned long off = vma->vm_pgoff << PAGE_SHIFT;
    //Mapping of registers
    unsigned long physical = phys_addr;
    unsigned long vsize = vma->vm_end - vma->vm_start;
    unsigned long psize = 0x1000; //One page is enough
    //printk("<1>start mmap of registers\n");
    if(vsize>psize)
      return -EINVAL;
    vma->vm_page_prot = pgprot_noncached(vma->vm_page_prot);
    remap_pfn_range(vma,vma->vm_start, physical >> PAGE_SHIFT , vsize, vma->vm_page_prot);
    if (vma->vm_ops)
      return -EINVAL; //It should never happen
    vma->vm_ops = &tst1_vm_ops;
    tst1_vma_open(vma); //This time no open(vma) was called
    //printk("<1>mmap of registers succeeded!\n");
    return 0;
}
*/

int sgl_map(const char __user *buf, size_t count, struct sg_table * sgt, struct page *** a_pages, int * a_n_pages)
{
    int res = 0;
    int n_pages;
    struct page ** pages = NULL;
    const unsigned long offset = ((unsigned long)buf) & (PAGE_SIZE-1);
    //Calculate number of pages
    n_pages = (offset + count + PAGE_SIZE - 1) >> PAGE_SHIFT;
    //Allocate the table for pages
    pages = kzalloc(sizeof(* pages) * n_pages,GFP_KERNEL);
    if(pages == NULL) {
        res = -ENOMEM;
        goto sglm_err1;
    }
    //Now pin the pages
    //res = get_user_pages_fast(buf, n_pages, rw == READ ? FOLL_WRITE : 0, pages);
    res = get_user_pages_fast(((unsigned long)buf & PAGE_MASK), n_pages, FOLL_WRITE, pages);
    if(res < n_pages) {
        int i;
        for(i=0; i<res; i++)
            put_page(pages[i]);
        res = -ENOMEM;
        goto sglm_err1;
    }
    //Now create the sg-list
    res = sg_alloc_table_from_pages(sgt, pages, n_pages, offset, count, GFP_KERNEL);
    if(res < 0)
        goto sglm_err2;
    *a_pages = pages;
    *a_n_pages = n_pages;
    return res;
sglm_err2:
    //Here we jump if we know that the pages are pinned
    {
        int i;
        for(i=0; i<n_pages; i++)
            put_page(pages[i]);
    }
sglm_err1:
    if(sgt) sg_free_table(sgt);
    if(pages) kfree(pages);
    * a_pages = NULL;
    * a_n_pages = 0;
    return res;
}

void sgl_unmap(struct sg_table * sgt, struct page ** pages, int n_pages)
{
    int i;
    //Free the sg list
    if(sgt->sgl)
        sg_free_table(sgt);
    //Unpin pages
    for(i=0; i < n_pages; i++) {
        set_page_dirty(pages[i]);
        put_page(pages[i]);
    }
}

ssize_t tst1_write(struct file *filp,
                   const char __user *buf,size_t count, loff_t *off)
{
    uint64_t val;
    ssize_t res;
    struct sg_table sgt;
    struct page ** pages = NULL;
    int n_pages;
    int nents;
    int i;
    int descs_size = 0;
    dma_addr_t da_descs = 0;
    Com1Desc * descs = NULL;
    volatile WzCom1Regs * regs;
    printk(KERN_ALERT "We are writing %ld bytes",count);
    regs = (volatile WzCom1Regs * ) fmem;
    memset(&sgt, 0, sizeof(sgt));
    //Now we map user pages, to allow passing them to DMA
    res = sgl_map(buf, count, &sgt, &pages, &n_pages);
    if(res < 0)
        return res;
    //Map the buffer
    nents = dma_map_sg(&my_pdev->dev, sgt.sgl, sgt.nents, DMA_TO_DEVICE);
    if(nents == 0) {
        res = - ENOMEM;
        goto write_err2;
    }
    printk(KERN_ALERT "DMA buffer setup - %d buffers used",nents);
    //SG table is ready, we may use it to prepare the DMA descriptors
    //We prepare a single group of descriptors for the whole transfer
    descs_size = sizeof(Com1Desc)+nents*sizeof(BufDesc);
    descs = kzalloc(descs_size,GFP_KERNEL);
    if(descs == NULL) {
        res = - ENOMEM;
        goto write_err3;
    }
    //Now we fill the descriptors
    descs->Length = nents;
    descs->Next = 0;
    for(i = 0; i < nents; i++) {
        descs->Descs[i].BufAddr = sg_dma_address(&sgt.sgl[i]);
        descs->Descs[i].Offset = 0;
        descs->Descs[i].Length = sg_dma_len(&sgt.sgl[i]);
    }
    //Now we need to map the descriptors
    da_descs = dma_map_single(&my_pdev->dev,(void *) descs,descs_size, DMA_TO_DEVICE);
    if(!da_descs) {
        res = - ENOMEM;
        goto write_err3;
    }
    //Synchronize the buffers (not tested yet)
    dma_sync_sg_for_device(&my_pdev->dev, sgt.sgl, sgt.nents, DMA_TO_DEVICE);
    dma_sync_single_for_device(&my_pdev->dev, da_descs, descs_size, DMA_TO_DEVICE);
    //Now we are ready to transfer
    //Now we do it without IRQ
    regs->FirstDesc = da_descs;
    mb();
    regs->Ctrl = COM1_CMD_DISIRQ;
    mb();
    regs->Ctrl = COM1_CMD_START;
    //Wait until transfer is finished
    while(1) {
        mb();
        if(!(regs->Status & 0x1000 )) break;
    };
    regs->Ctrl = COM1_CMD_ACKIRQ;
    mb();
    regs->Ctrl = COM1_CMD_DISIRQ;
    mb();
    //Transfer done, let's clean up
    dma_unmap_single(&my_pdev->dev, da_descs, descs_size, DMA_TO_DEVICE);
    //We assume, that all data were transferred
    res = count;
write_err3:
    dma_unmap_sg(&my_pdev->dev, sgt.sgl, nents, DMA_TO_DEVICE);
write_err2:
    //Unmap the user pages
    sgl_unmap(&sgt, pages, n_pages);
    if(pages) kfree(pages);
    return res;
}

static long tst1_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    volatile WzCom1Regs * r = (volatile WzCom1Regs *) fmem;
    printk(KERN_ALERT "I'm in my IOCTL");
    switch(cmd) {
    case COM_IO_RUN:
        printk(KERN_ALERT "I'm in my IOCTL - IO_RUN");
        //Now this procedure prepares a single DMA transfer in a blocking mode (without interrupts)
        //Let's create 10 buffers, and fill them with data: 0x10000*buf_num+smp_num
        {
            int i,j,k;
            uint32_t * bufs[10];
            int sizes[10] = {1243,321,673,901,511,633,457,811,427,631};
            Com1Desc * desc[3];
            for(i=0; i<10; i++) {
                bufs[i] = kzalloc(sizeof(uint32_t)*sizes[i],GFP_KERNEL);
                for(j = 0; j<sizes[i]; j++) bufs[i][j] = i*0x10000 + j;
            }
            //We group those buffers into three segments with lengths: 2 buffers,5 buffers and 3 buffers
            desc[0]=kzalloc(sizeof(Com1Desc)+2*sizeof(BufDesc),GFP_KERNEL);
            desc[1]=kzalloc(sizeof(Com1Desc)+5*sizeof(BufDesc),GFP_KERNEL);
            desc[2]=kzalloc(sizeof(Com1Desc)+3*sizeof(BufDesc),GFP_KERNEL);
            desc[0]->Length = 2;
            desc[0]->Next = virt_to_phys(desc[1]);
            desc[0]->Descs[0].BufAddr = virt_to_phys(bufs[0]);
            desc[0]->Descs[0].Length = sizes[0]*sizeof(uint32_t);
            desc[0]->Descs[0].Offset = 0;
            desc[0]->Descs[1].BufAddr = virt_to_phys(bufs[1]);
            desc[0]->Descs[1].Length = sizes[1]*sizeof(uint32_t);
            desc[0]->Descs[1].Offset = 0;
            desc[1]->Length = 5;
            desc[1]->Next = virt_to_phys(desc[2]);
            desc[1]->Descs[0].BufAddr = virt_to_phys(bufs[2]);
            desc[1]->Descs[0].Length = sizes[2]*sizeof(uint32_t);
            desc[1]->Descs[0].Offset = 0;
            desc[1]->Descs[1].BufAddr = virt_to_phys(bufs[3]);
            desc[1]->Descs[1].Length = sizes[3]*sizeof(uint32_t);
            desc[1]->Descs[1].Offset = 0;
            desc[1]->Descs[2].BufAddr = virt_to_phys(bufs[4]);
            desc[1]->Descs[2].Length = sizes[4]*sizeof(uint32_t);
            desc[1]->Descs[2].Offset = 0;
            desc[1]->Descs[3].BufAddr = virt_to_phys(bufs[5]);
            desc[1]->Descs[3].Length = sizes[5]*sizeof(uint32_t);
            desc[1]->Descs[3].Offset = 0;
            desc[1]->Descs[4].BufAddr = virt_to_phys(bufs[6]);
            desc[1]->Descs[4].Length = sizes[6]*sizeof(uint32_t);
            desc[1]->Descs[4].Offset = 0;
            desc[2]->Length = 3;
            desc[2]->Next = 0;
            desc[2]->Descs[0].BufAddr = virt_to_phys(bufs[7]);
            desc[2]->Descs[0].Length = sizes[7]*sizeof(uint32_t);
            desc[2]->Descs[0].Offset = 0;
            desc[2]->Descs[1].BufAddr = virt_to_phys(bufs[8]);
            desc[2]->Descs[1].Length = sizes[8]*sizeof(uint32_t);
            desc[2]->Descs[1].Offset = 0;
            desc[2]->Descs[2].BufAddr = virt_to_phys(bufs[9]);
            desc[2]->Descs[2].Length = sizes[9]*sizeof(uint32_t);
            desc[2]->Descs[2].Offset = 0;
            r->FirstDesc = virt_to_phys(desc[0]);
            mb();
            r->Ctrl = COM1_CMD_DISIRQ;
            mb();
            r->Ctrl = COM1_CMD_START;
            //Wait until transfer is finished
            while(1) {
                mb();
                if(!(r->Status & 0x1000 )) break;
            };
            r->Ctrl = COM1_CMD_ACKIRQ;
            mb();
            r->Ctrl = COM1_CMD_DISIRQ;
            mb();
            for(i=0; i<10; i++) kfree(bufs[i]);
            for(i=0; i<3; i++) kfree(desc[i]);
            return 0;
        }
    }
    return -EINVAL;
}

static int tst1_probe(struct pci_dev * pdev, const struct pci_device_id *ent)
{
    unsigned long mmio_start, mmio_end, mmio_flags, mmio_len;
    int res = 0;
    //We can service only one device
    if(my_pdev)
        return -ENODEV;
    //struct resource * resptr = NULL;
    res =  pci_enable_device(pdev);
    if (res) {
        dev_err(&pdev->dev, "Can't enable PCI device, aborting\n");
        res = -ENODEV;
        goto err1;
    }
    mmio_start = pci_resource_start(pdev, 0);
    mmio_end = pci_resource_end(pdev, 0);
    mmio_flags = pci_resource_flags(pdev, 0);
    mmio_len = pci_resource_len(pdev, 0);
    irq = pdev->irq;
    if(irq<0) {
        printk(KERN_ERR "Error reading the IRQ number: %d.\n",irq);
        res=irq;
        goto err1;
    }
    printk(KERN_ALERT "Connected IRQ=%d\n",irq);
    //printk("<1> mmio_start=%x, mmio_end=%x, mmio_len=%x, 	irq=%d\n",mmio_start,mmio_end,mmio_len,irq);
    /* make sure PCI base addr 1 is MMIO */
    if (!(mmio_flags & IORESOURCE_MEM)) {
        dev_err(&pdev->dev, "region #1 not an MMIO resource, aborting\n");
        res = -ENODEV;
        goto err1;
    }
    res = pci_request_regions(pdev, DEVICE_NAME);
    if(res) {
        printk(KERN_ERR "Error requesting PCI regions.\n");
        goto err1;
    }
    pci_set_master(pdev);
    class_my_tst = class_create(THIS_MODULE, "my_tst");
    if (IS_ERR(class_my_tst)) {
        printk(KERN_ERR "Error creating my_tst class.\n");
        res=PTR_ERR(class_my_tst);
        goto err1;
    }
    /* Alocate device number */
    res=alloc_chrdev_region(&my_dev, 0, 1, DEVICE_NAME);
    if(res) {
        printk ("<1>Alocation of the device number for %s failed\n",
                DEVICE_NAME);
        goto err1;
    };
    my_cdev = cdev_alloc( );
    if(my_cdev == NULL) {
        printk ("<1>Allocation of cdev for %s failed\n",
                DEVICE_NAME);
        goto err1;
    }
    my_cdev->ops = &Fops;
    my_cdev->owner = THIS_MODULE;
    /* Add character device */
    res=cdev_add(my_cdev, my_dev, 1);
    if(res) {
        printk ("<1>Registration of the device number for %s failed\n",
                DEVICE_NAME);
        goto err1;
    };
    /* Create pointer needed to access registers */
    phys_addr = mmio_start;
    printk(KERN_ALERT "Connected registers at %lx\n",phys_addr);
    fmem = ioremap_nocache(phys_addr, 0x1000); //One page should be enough
    if(!fmem) {
        printk ("<1>Mapping of memory for %s registers failed\n",
                DEVICE_NAME);
        res= -ENOMEM;
        goto err1;
    }
    device_create(class_my_tst,NULL,my_dev,NULL,"my_com%d",MINOR(my_dev));
    printk ("<1>%s The major device number is %d.\n",
            "Successful registration.",
            MAJOR(my_dev));
    {
        WzCom1Regs * r = (WzCom1Regs *) fmem;
        printk(KERN_ALERT "Id=%llx\n",r->Id);
        r->Test=1;
        printk(KERN_ALERT "Id=%llx\n",r->Id);
        printk(KERN_ALERT "Stat=%llx\n",r->Test);
    }
    my_pdev = pdev;
    return 0;
err1:
    tst1_remove(pdev);
    return res;
}

struct pci_driver my_driver = {
    .name = DEVICE_NAME,
    .id_table = tst1_pci_tbl,
    .probe = tst1_probe,
    .remove = tst1_remove,
};

static int __init my_init(void)
{
    return pci_register_driver(&my_driver);
}
static void __exit my_exit(void)
{
    pci_unregister_driver(&my_driver);
}

module_init(my_init);
module_exit(my_exit);

